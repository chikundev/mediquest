--[[
    Set up the world variables
  ]]--

world = {
    gravity             = 3500,
    terminalVelocity    = 800,      -- Limit on how fast objects can fall
    airFriction         = 0.9,
    groundFriction      = 1.0,
    deathCounter        = 0,
    werd                = "",
    haveMedicine        = false,
    gameEnding          = false,
    fadeTimer           = 0,
    levelLoaded         = false
}


function world.update(dt)
    if world.gameEnding then
        world.fadeTimer = world.fadeTimer + 150 * dt
    end

    local tmp = string.gsub(levelName, "maps/", "", 1)
    if tmp == "level07" then
        world.groundFriction = 0.2
    elseif tmp == "level08" then
        world.groundFriction = 0.4
    else
        world.groundFriction = 1.0
    end
    world.airFriction = world.groundFriction * 0.9

    if tmp == "levelMedicine" then
        local bossCheck = false
        for k, v in ipairs(enemies) do
            if v.type == enemyTypes.boss then
                bossCheck = true
            end
        end

        if medicine ~= nil then
            if bossCheck then
                medicine.canGet = false
            else
                medicine.canGet = true
            end

            if medicine.canGet and checkCollision(player, medicine) then
                medicine = nil
                world.haveMedicine = true
            end
        end
    end
end
