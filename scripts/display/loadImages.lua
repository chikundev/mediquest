--[[
    Loads all required image files into memory
  ]]--
--[[
gfx = {
    player = {
        main    = love.graphics.newImage("gfx/player/main.png"),    -- Default standing pose
        falling = love.graphics.newImage("gfx/player/falling.png"), -- When player is falling
        walk    = {     -- Player's walk cycle
            love.graphics.newImage("gfx/player/walk_1.png"),
            love.graphics.newImage("gfx/player/walk_2.png"),
            love.graphics.newImage("gfx/player/walk_3.png"),
            love.graphics.newImage("gfx/player/walk_2.png"),
        },
        hit     = {     -- Player incurs damage
            love.graphics.newImage("gfx/player/hit_1.png"),
            love.graphics.newImage("gfx/player/hit_2.png"),
            love.graphics.newImage("gfx/player/hit_3.png"),
        },
    },
    enemy = {           -- Monster sprites
        grunt   = love.graphics.newImage("gfx/enemies/spider.png"),         -- Spider
        skelly  = love.graphics.newImage("gfx/enemies/skeleton.png"),       -- Skeleton
        wasp    = love.graphics.newImage("gfx/enemies/wasp.png"),           -- Wasp
        goblin  = love.graphics.newImage("gfx/enemies/goblin.png"),         -- Goblin
        floaty  = love.graphics.newImage("gfx/enemies/eyeball.png"),        -- Floating Eye
        dkskel  = love.graphics.newImage("gfx/enemies/darkskeleton.png"),   -- Dark Skeleton
        boss    = love.graphics.newImage("gfx/enemies/boss.png"),           -- Boss
    },
    totem = {           -- Totem sprite
        main    = love.graphics.newImage("gfx/totem/main.png"),
        glow    = {     -- Blinking lights on totem
            love.graphics.newImage("gfx/totem/glow_1.png"),
            love.graphics.newImage("gfx/totem/glow_2.png"),
            love.graphics.newImage("gfx/totem/glow_3.png"),
        },
    },
    speech = love.graphics.newImage("gfx/window/speech.png"),
}]]

gfx.player.walk = {
    gfx.player.walk_1,
    gfx.player.walk_2,
    gfx.player.walk_3,
    gfx.player.walk_2
}

gfx.player.hit = {
    gfx.player.hit_1,
    gfx.player.hit_2,
    gfx.player.hit_3
}

gfx.enemy = {
    grunt     = gfx.enemies.spider,
    skelly    = gfx.enemies.skeleton,
    wasp      = gfx.enemies.wasp,
    goblin    = gfx.enemies.goblin,
    floaty    = gfx.enemies.eyeball,
    dkskel    = gfx.enemies.darkskeleton,
    boss      = gfx.enemies.boss
}

gfx.totem.glow = {
    gfx.totem.glow_1,
    gfx.totem.glow_2,
    gfx.totem.glow_3,
}

gfx.speech  = gfx.window.speech
