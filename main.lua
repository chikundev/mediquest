--[[
    MediQuest: A game with not-so-many checkpoints

    Lolshouse Game Dev, 2013 - For LD72 #28
  ]]--

function love.system.getOS()

    return "Android"

end

function love.load()

    isOUYA = false

    gameCanvas = love.graphics.newCanvas(854, 480)

    love.mouse.setVisible(false)

    love.graphics.setDefaultFilter('nearest')

    require "scripts/bindings"
    require "scripts/newLoaders/gfx"

    require "scripts/onStartup"         -- Runs all required startup scripts

    require "scripts/iGrid"

    resAllLoaded = true

    -- NOTE: Press 'E' to enter enemy test
    map.load("maps/level00")            -- Loads first map

    newState = require "scripts/splash"

    newState:create()

    controllerLast = false

end

function love.update(dt)
    if dt > 1/15 then
        dt = 1/15
    end

    if gameLoaded then
        getVert = nil
        currentForce = nil
        toJump = nil
        totemPlace = nil

        contr = "no controller connected"

        local joys = love.joystick.getJoysticks()

        for key, value in ipairs(joys) do

            contr = "kay " .. tostring(value:isGamepad())

            if value:isGamepad() then

                if math.abs(value:getGamepadAxis('leftx')) > 0.2 then

                    currentForce = value:getGamepadAxis('leftx')

                    controllerLast = true
                end

                if math.abs(value:getGamepadAxis('lefty')) > 0.2 then

                    getVert = value:getGamepadAxis('lefty')

                    controllerLast = true
                end

                if value:isGamepadDown('a') then

                    toJump = true

                    controllerLast = true
                end

                if value:isGamepadDown('x') then

                    totemPlace = true

                    controllerLast = true
                end

                if value:isGamepadDown('y') then

                    if not xButton then
                        player.totemMove()

                        controllerLast = true

                        xButton = true

                    end

                else

                    xButton = false
                end
            end
        end

        if not levelLoaded then
            levelLoaded     = true
            player.xSpeed   = 0
            player.ySpeed   = 0
        end
        player.update(dt)       -- Run updates on the player
        fBlockUpdate(dt)        -- Run updates on falling blocks
        enemy.update(dt)
        totem.place()
        totem.update(dt)
        view.update()           -- Update the view's positioning
        world.update(dt)
    elseif resAllLoaded then
        newState:update(dt)
    end
end

function love.draw()
    -- Round view x and y to prevent weird artifacts between pixels
    local vx, vy
    vx = math.round(view.x) ; vy = math.round(view.y)

    love.graphics.setCanvas(gameCanvas)

    love.graphics.setColor(0, 0, 0)

    love.graphics.rectangle('fill', 0, 0, w.getWidth(), w.getHeight())

    love.graphics.setColor(255, 255, 255)

    if gameLoaded then

        map.draw(vx, vy)        -- Draw the current level map
        totem.draw(vx, vy)      -- Draw the totem if it can
        player.draw(vx, vy)     -- Draw the player
        enemy.draw(vx, vy)      -- Draw all enemies
        map.drawMist(vx, vy)
        gui.draw()              -- Draw the GUI (MUST be last in chain)

    elseif resAllLoaded then

        newState:draw()

    end

    love.graphics.setCanvas()

    rescale()
end

function love.keypressed(key)

    controllerLast = false

    -- If escape is pressed end the game
    if key == "escape" then
        love.event.quit()       -- End the game
    elseif key == "lshift" then
        player.totemMove()
    elseif key == "f11" then
        love.window.setFullscreen(not love.window.getFullscreen(), 'desktop')
    end
end

function rescale()

    local hScal, vScal = love.window.getWidth() / gameCanvas:getWidth(),
        love.window.getHeight() / gameCanvas:getHeight()

    local x, y = 0, 0

    if hScal > vScal then

        x = (w.getWidth() / 2) - (854 * vScal / 2)

    else

        y = (w.getHeight() / 2) - (480 * hScal / 2)

    end

    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(gameCanvas, x, y, 0, math.min(hScal, vScal))

end
