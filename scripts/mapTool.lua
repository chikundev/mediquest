--[[
    Map loading system
  ]]--

map     = { };
internal= { };  -- Houses internal functions for this script
levelName = "";
local mapData, tiles, images, rects;

function map.load(file, tot)
    player.setup()
    tiles   = { };
    images  = { };
    rects   = { };
    quads   = { };
    coins   = { };
    diamonds= { };
    spikes  = { };
    enemies = { };
    death   = { };
    scenery = { };
    ladders = { };
    health  = { };
    fBlocks = { };
    mist    = { };
    medicine= nil;
    levelEnd= nil;
    levelLoaded = false;
    -- Load data from map file
    levelName = file
    mapData = require(file);
    startMusic(string.gsub(file, "maps/", "", 1))

    -- Load images from tilesets
    for k, v in ipairs(mapData.tilesets) do
        local i, marg, vspac;
        --images[k] = love.graphics.newImage(v.image);
        images[k] = getResourceFromString(v.image)
        -- Load quads from tileset
        i       = v.firstgid;
        vspac   = v.margin;
        for h=0, math.ceil((v.imageheight - v.spacing) / v.tileheight)-1 do
            marg    = v.margin;
            for w=0, math.ceil((v.imagewidth - v.spacing * v.tilewidth) / v.tilewidth)-1 do
                quads[i] = love.graphics.newQuad(marg + (w * v.tilewidth), vspac + (h * v.tileheight), v.tilewidth, v.tileheight, v.imagewidth, v.imageheight);
                marg    = marg + v.spacing;
                i       = i + 1;
            end
            vspac   = vspac + v.spacing;
        end
    end

    -- Load rectangles
    for k, v in ipairs(mapData.layers) do
        if v.name == "collisions" then
            for k, v in ipairs(v.objects) do
                rects[#rects+1] = {
                    x = v.x;
                    y = v.y;
                    width = v.width;
                    height = v.height;
                }
            end
        elseif v.name == "coins" then
            for k, v in ipairs(v.objects) do
                local v1, v2, w, h;
                v1, v2, w, h = quads[v.gid]:getViewport();
                coins[#coins+1] = {
                    gid = v.gid;
                    width = w;
                    height = h;
                    x = v.x;
                    y = v.y - h;
                    }
            end
        elseif v.name == "diamonds" then
            for k, v in ipairs(v.objects) do
                local v1, v2, w, h;
                v1, v2, w, h = quads[v.gid]:getViewport();
                diamonds[#diamonds+1] = {
                    gid = v.gid;
                    width = w;
                    height = h;
                    x = v.x;
                    y = v.y - h;
                    }
            end
        elseif v.name == "health" and not world.haveMedicine then
            for k, v in ipairs(v.objects) do
                local v1, v2, w, h;
                v1, v2, w, h = quads[v.gid]:getViewport();
                health[#health+1] = {
                    gid = v.gid;
                    width = w;
                    height = h;
                    x = v.x;
                    y = v.y - h;
                    }
            end
        elseif v.name == "scenery" then
            for k, v in ipairs(v.objects) do
                local v1, v2, w, h;
                v1, v2, w, h = quads[v.gid]:getViewport();
                scenery[#scenery+1] = {
                    gid = v.gid;
                    width = w;
                    height = h;
                    x = v.x;
                    y = v.y - h;
                    }
            end
        elseif v.name == "mist" then
            for k, v in ipairs(v.objects) do
                local v1, v2, w, h;
                v1, v2, w, h = quads[v.gid]:getViewport();
                mist[#mist+1] = {
                    gid = v.gid;
                    width = w;
                    height = h;
                    x = v.x;
                    y = v.y - h;
                    }
            end
        elseif v.name == "death" then
            for k, v in ipairs(v.objects) do
                death[#death+1] = {
                    width = v.width;
                    height = v.height;
                    x = v.x;
                    y = v.y;
                    }
            end
        elseif v.name == "ladders" then
            for k, v in ipairs(v.objects) do
                ladders[#ladders+1] = {
                    width = v.width;
                    height = v.height;
                    x = v.x;
                    y = v.y;
                    }
            end
        elseif v.name == "spikes" then
            for k, v in ipairs(v.objects) do
                local v1, v2, w, h;
                v1, v2, w, h = quads[v.gid]:getViewport();
                spikes[#spikes+1] = {
                    gid = v.gid;
                    width = w;
                    height = h;
                    x = v.x;
                    y = v.y - h;
                    }
            end
        elseif v.name == "fallingBlocks" then
            for k, v in ipairs(v.objects) do
                local v1, v2, w, h;
                v1, v2, w, h = quads[v.gid]:getViewport();
                fBlocks[#fBlocks+1] = {
                    gid = v.gid;
                    width = w;
                    height = h;
                    x = v.x;
                    y = v.y - h;
                    startY = v.y - h;
                    isFalling = false,
                    fallTimer = nil
                    }
            end
        elseif v.name == "enemies" and not world.haveMedicine then
            for k, v in ipairs(v.objects) do
                local v1, v2, w, h;
                v1, v2, w, h = quads[v.gid]:getViewport();
                newEnemy(enemyTypes[v.type], v.x, v.y - h, w, h)
            end
        elseif v.name == "important" then
            local tmp1, tmp2, face
            tmp1 = "playerStart"
            tmp2 = "levelEnd"
            face = 1
            if world.haveMedicine then
                tmp1 = "levelEnd"
                tmp2 = "playerStart"
                face = -1
            end
            for k, v in ipairs(v.objects) do
                if v.name == tmp1 and tot == nil then
                    player.x = v.x
                    player.y = v.y + (64 - player.height)
                    player.facing = face
                elseif v.name == tmp2 then
                    levelEnd = {
                        x       = v.x,
                        y       = v.y,
                        width   = v.width,
                        height  = v.height,
                        nextLvl = v.properties.nextLevel
                    }
                end
                if v.name == "medicine" then
                    local v1, v2, w, h;
                    v1, v2, w, h = quads[v.gid]:getViewport();
                    medicine = {
                        x       = v.x,
                        y       = v.y - h,
                        width   = w,
                        height  = h,
                        gid     = v.gid,
                        canGet  = false
                    }
                end
            end
        end
    end
end

function map.clear()
    rects = nil
end

function map.touches(obj)
    -- Returns true if obj collides with any rectangle in the map
    local returnValue   = false;
    for k, v in ipairs(rects) do
        if checkCollision(obj, v) then
            returnValue = true;
        end
    end
    for k, v in ipairs(fBlocks) do
        if checkCollision(obj, v) then
            if v.fallTimer == nil and not v.isFalling and obj == player then
                love.audio.stop(sndBlockFall)
                love.audio.play(sndBlockFall)
                v.fallTimer = 0.2
            end
            returnValue = true;
        end
    end
    return returnValue;
end

function map.getWidth()
    return mapData.width * mapData.tilewidth
end

function map.getHeight()
    return mapData.height * mapData.tileheight
end

function map.restart()
    if totem.isActivated then
        map.load(totem.level, "nope")
        player.x = totem.x + (totem.width / 2) - (player.width / 2)
        player.y = totem.y - (player.height - totem.height)
        love.audio.play(sndTotemSave)
        if world.haveMedicine then
            player.facing = -1
        else
            player.facing = 1
        end
    else
        map.load("maps/level01")
    end
end

function checkCollision(obj1, obj2)
    return (obj1.x < obj2.x + obj2.width and
         obj2.x < obj1.x + obj1.width and
         obj1.y < obj2.y + obj2.height and
         obj2.y < obj1.y + obj1.height);
end

function map.draw(xOffset, yOffset)
    for k, v in ipairs(scenery) do
        love.graphics.draw(internal.getImage(v.gid), quads[v.gid], v.x - xOffset * 0.9, v.y - yOffset * 0.9);
    end
    -- Draw all tiles of the map
    local tmp_x, tmp_y;
    for k, v in ipairs(mapData.layers) do
        multi = 1
        if v.type == "tilelayer" then
            if v.name == "background" then
                multi = 0.9
            end
            for k, v in ipairs(v.data) do
                tmp_y = math.floor((k - 1) / mapData.width)
                tmp_x = k - (tmp_y * mapData.width) - 1;
                if v > 0 then
                    local qu = {quads[v]:getViewport()}
                    local pos = {
                        x = tmp_x * mapData.tilewidth - xOffset * multi,
                        y = tmp_y * mapData.tileheight - yOffset * multi,
                        width = qu[3],
                        height = qu[4]
                    }
                    if checkCollision(pos, {x = 0, y = 0, width = 854, height = 480}) then
                        love.graphics.draw(internal.getImage(v), quads[v], pos.x, pos.y);
                    end
                end
            end
        end
    end
    for k, v in ipairs(coins) do
        if checkCollision(v, view) then
            love.graphics.draw(internal.getImage(v.gid), quads[v.gid], v.x - xOffset, v.y - yOffset);
        end
    end
    for k, v in ipairs(diamonds) do
        if checkCollision(v, view) then
            love.graphics.draw(internal.getImage(v.gid), quads[v.gid], v.x - xOffset, v.y - yOffset);
        end
    end
    for k, v in ipairs(health) do
        if checkCollision(v, view) then
            love.graphics.draw(internal.getImage(v.gid), quads[v.gid], v.x - xOffset, v.y - yOffset);
        end
    end
    for k, v in ipairs(spikes) do
        if checkCollision(v, view) then
            love.graphics.draw(internal.getImage(v.gid), quads[v.gid], v.x - xOffset, v.y - yOffset);
        end
    end
    for k, v in ipairs(fBlocks) do
        if checkCollision(v, view) then
            love.graphics.draw(internal.getImage(v.gid), quads[v.gid], v.x - xOffset, v.y - yOffset);
        end
    end
    if medicine ~= nil then
        if not medicine.canGet then
            love.graphics.setColor(255,255,255,128)
        end
        love.graphics.draw(internal.getImage(medicine.gid), quads[medicine.gid], medicine.x - xOffset, medicine.y - yOffset);
        love.graphics.setColor(255,255,255,255)
    end
end

function map.drawMist(vx, vy)
    for k, v in ipairs(mist) do
        if checkCollision(v, view) then
            love.graphics.draw(internal.getImage(v.gid), quads[v.gid], v.x - vx, v.y - vy);
        end
    end
end

function internal.getImage(v)
    -- Figures out which image to use based on tile number
    local num;
    num     = #mapData.tilesets;
    while (v < mapData.tilesets[num].firstgid) do
        num = num - 1;
    end
    return images[num];
end

function fBlockUpdate(dt)
    for k, v in ipairs(fBlocks) do
        if v.fallTimer ~= nil and not v.isFalling then
            v.fallTimer = v.fallTimer - dt
            if v.fallTimer < 0 then
                v.fallTimer = 0
                v.isFalling = true
            end
        end
        if v.isFalling then
            v.y = v.y + 200 * dt
            v.fallTimer = v.fallTimer + 1 * dt
            if v.fallTimer > 6 then
                v.y = v.startY
                v.isFalling = false
                v.fallTimer = nil
            end
        end
    end
end

-- Convert resource from string to actual data
-- eg. 'gfx.shrek' to gfx.shrek's image data
-- Used by mapTool
function getResourceFromString(resource)

    -- Remove unneccessary parts of filename
    resource = resource:gsub(".jpg", "")
    resource = resource:gsub(".ogg", "")
    resource = resource:gsub(".png", "")
    resource = resource:gsub("%.%./", "")

    -- Replace slashes with periods to simplify loading
    resource = resource:gsub("/", "\\")

    -- Convert current string to table of strings
    tab = { }
    for word in resource:gmatch("([^\\]+)") do
        tab[#tab + 1] = word
    end

    -- Recursively access deeper parts of table containing resource
    for key, value in ipairs(tab) do
        if key == 1 then
            resource = _G[value]
        else
            resource = resource[value]
        end
    end

    -- Return the found resource
    return(resource)

end



-- Does a deep copy of a table
-- Used by mapTool
function deepcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[deepcopy(orig_key)] = deepcopy(orig_value)
        end
        setmetatable(copy, deepcopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return(copy)
end
