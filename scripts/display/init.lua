--[[
    Load GUI-centric settings
  ]]--

-- Set general graphics state
love.graphics.setBackgroundColor(160, 160, 160)     -- Set background colour to grey

-- Set up view
view = {
    x       = 0,
    y       = 0,
    width   = 854,
    height  = 480
}

function view.update()
    view.x = player.x - (view.width / 2) + (player.width / 2)
    if view.x < 0 then
        view.x = 0
    elseif view.x > map.getWidth() - view.width then
        view.x = map.getWidth() - view.width
    end
    view.y = player.y - (view.height / 2) + (player.height / 2)
    if view.y < 0 then
        view.y = 0
    elseif view.y > map.getHeight() - view.height then
        view.y = map.getHeight() - view.height
    end
end

-- Set up the GUI
gui = {
    -- OH LOOK, IT'S FUCKING NOTHING
}

function gui.draw()
    -- Draw death counter
    local tmp
    if world.deathCounter == 0 then
        tmp = "YOU HAVE NOT YET DIED"
    elseif world.deathCounter == 1 then
        tmp = "YOU HAVE DIED ONCE"
    else
        tmp = "YOU HAVE DIED " .. world.deathCounter .. " TIMES"
    end

    if not (levelName == "maps/level00" and not world.haveMedicine) then
        -- Draw FPS
        love.graphics.setFont(fontScore)
        love.graphics.setColor(0, 0, 0)
        love.graphics.print("SCORE: " .. player.score, 10, 8)
        love.graphics.setColor(255, 255, 255)
        love.graphics.print("SCORE: " .. player.score, 8, 6)
        love.graphics.setFont(fontScore)
        love.graphics.setColor(0, 0, 0)

        local tmpY = 480 - fontScore:getHeight(tmp) - 10
        if s.getOS() == "Android" and not isOUYA then
            tmpY = 6
        end
        love.graphics.print(tmp, 854 - fontScore:getWidth(tmp) - 8, tmpY + 2)
        love.graphics.setColor(255, 255, 255)
        love.graphics.print(tmp, 854 - fontScore:getWidth(tmp) - 10, tmpY)
        -- Draw health bar
        love.graphics.setColor(200, 0, 0)
        love.graphics.rectangle("fill", 8, 48, 200, 16)
        love.graphics.setColor(0, 200, 100)
        love.graphics.rectangle("fill", 8, 48, player.health * 2, 16)
        love.graphics.setColor(0, 0, 0)
        love.graphics.rectangle("line", 8, 48, 200, 16)
    end

    if levelName == "maps/level00" and not world.haveMedicine then
        -- Reset colour
        love.graphics.setColor(255, 255, 255, 192)
        love.graphics.draw(gfx.speech, 22, 20)
        love.graphics.setColor(0, 0, 0, 255)
        love.graphics.setFont(fontHeader)
        local tmp1, tmp2
            tmp1 = "CONTROLS"
            tmp2 = "Use Left, Right, Up & Down to move.\nPress Spacebar to jump.\n" ..
            "Press Control to place your checkpoint. YOU CAN ONLY DO THIS ONCE.\n" ..
            "If you die you will either spawn at the totem or the first level if " ..
            "it has not been placed.\nPress Shift to return to totem."
        if controllerLast then

            tmp2 = "Use Left Analog Stick to move.\nPress A to jump.\n" ..
            "Press X to place your checkpoint. YOU CAN ONLY DO THIS ONCE.\n" ..
            "If you die you will either spawn at the totem or the first level if " ..
            "it has not been placed.\nPress Y to return to totem."

        end
        love.graphics.print(tmp1, 31, 29)
        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.print(tmp1, 30, 28)
        love.graphics.setFont(fontText)
        love.graphics.setColor(0, 0, 0, 255)
        love.graphics.printf(tmp2, 31, 59, 332, 'left')
        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.printf(tmp2, 30, 58, 332, 'left')


    love.graphics.setColor(255, 255, 255, 192)
        love.graphics.draw(gfx.speech, 472, 20)
        love.graphics.setColor(0, 0, 0, 255)
        love.graphics.setFont(fontHeader)
            tmp1 = "INSTRUCTIONS"
            tmp2 = "Your poor grandma is sick and she needs you to retrieve a very " ..
                "strong medicine for her. You must go on a quest to find it and " ..
                "bring it back. She has given you a teleportation totem and stone, " ..
                "so that if you fall to danger, you will be resurrected.\n" ..
                "... Move right to begin"
        love.graphics.print(tmp1, 481, 29)
        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.print(tmp1, 480, 28)
        love.graphics.setFont(fontText)
        love.graphics.setColor(0, 0, 0, 255)
        love.graphics.printf(tmp2, 481, 59, 332, 'left')
        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.printf(tmp2, 480, 58, 332, 'left')
    end
    if world.gameEnding then
        if world.fadeTimer < 255 then
            love.graphics.setColor(0, 0, 0, world.fadeTimer)
        else
            love.graphics.setColor(0, 0, 0, 255)
        end
        love.graphics.rectangle("fill", 0, 0, 854, 480)
    end
    love.graphics.setColor(255, 255, 255, 255)
    local tmp = 980 - world.fadeTimer
    love.graphics.setFont(fontScore)
    love.graphics.printf("YOU HAVE DELIVERED THE MEDICINE\nAND SAVED THE DAY", 0, tmp, 854, 'center')
    love.graphics.printf("THANK YOU FOR PLAYING!", 0, tmp+540, 854, 'center')
    love.graphics.setFont(fontScore)
    love.graphics.printf("PROGRAMMING", 0, tmp+1080, 854, 'center')
    love.graphics.setFont(fontHeader)
    love.graphics.printf("Chris Alderton\nJosef Frank", 0, tmp+1120, 854, 'center')
    love.graphics.setFont(fontScore)
    love.graphics.printf("LEVEL DESIGNER", 0, tmp+1620, 854, 'center')
    love.graphics.setFont(fontHeader)
    love.graphics.printf("Mathew Dwyer", 0, tmp+1660, 854, 'center')
    love.graphics.setFont(fontScore)
    love.graphics.printf("SOUND EFFECTS", 0, tmp+2160, 854, 'center')
    love.graphics.setFont(fontHeader)
    love.graphics.printf("Josef Frank", 0, tmp+2200, 854, 'center')
    love.graphics.setFont(fontScore)
    love.graphics.printf("BACKGROUND MUSIC", 0, tmp+2740, 854, 'center')
    love.graphics.setFont(fontHeader)
    love.graphics.printf("Cohen Dennis (soundcloud.com/elchickenman-1)\nKevin MacLeod (incompetech.com)", 0, tmp+2780, 854, 'center')
    love.graphics.setFont(fontScore)
    love.graphics.printf("ART ASSETS", 0, tmp+3280, 854, 'center')
    love.graphics.setFont(fontHeader)
    love.graphics.printf("See sources.txt", 0, tmp+3320, 854, 'center')
    love.graphics.setFont(fontScore)
    love.graphics.printf("THIS\n\nGAME\n\nWILL\n\nNOW\n\nEND\n\n\nTHANK\n\nYOU\n\nSO\n\nVERY\n\nMUCH"..
            "\n\n\n\n\n\n\n\n<3 <3 <3 <3 <3", 0, tmp+4000, 854, 'center')
    if world.fadeTimer > 6000 then
        love.event.quit()
    end
end
