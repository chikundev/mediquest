--[[
    Performed on game startup, loads all required scripts and files
  ]]--

require "scripts/display/init"          -- Load GUI-centric parts for the game
require "scripts/display/loadFonts"     -- Load fonts for the game
require "scripts/display/loadImages"    -- Load most images for the game

require "scripts/extraMaths"            -- Extra mathematical functions
require "scripts/mapTool"               -- Load the map loader
require "scripts/world/init"            -- Load the world settings
require "scripts/world/loadSounds"      -- Load the sounds required in-game

require "scripts/player/init"           -- Create and load the player
require "scripts/enemy/init"            -- Create and load enemies
require "scripts/items/totem"           -- Create and load totem
